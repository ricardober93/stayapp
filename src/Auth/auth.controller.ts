import { Controller, HttpCode, HttpException, HttpStatus, Post, Req } from '@nestjs/common';
import { Request } from 'express';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  @HttpCode(200)
  logIn(@Req() request: Request) {
    const body = request.body;
    if (body.user === 'Ricardo') {
        return this.authService.findUser(body);
      } else {
          throw new HttpException('Las credenciales no son correctas', HttpStatus.BAD_REQUEST);
      }
    }
   
  }
