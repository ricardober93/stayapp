import { useAppSelector } from '../../hook/useStore';
import { useGetHousesQuery } from '../services/stayApp.service';

export default function useList() {
  useGetHousesQuery()
  const list = useAppSelector(state => state.stayApp.house)
  return {
    list,
  };
}
