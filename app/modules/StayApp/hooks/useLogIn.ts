import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useAppSelector } from './../../hook/useStore';

export default function useLogIn() {
  const router = useRouter();
  const user = useAppSelector((state) => state.user);

  useEffect(() => {
    if (!user.isLoggin) {
      router.push('/login');
    }
  }, []);

  return {
    user,
  };
}
