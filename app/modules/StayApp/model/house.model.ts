export interface IHouse {
    id?: number,
    title: string,
    city: string,
    mount: number,
    select: boolean
}