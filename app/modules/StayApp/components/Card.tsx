import {
  Flex, Text
} from '@chakra-ui/react';
import Image from 'next/image';
import login from '../../../pages/login';
import { useAppDispatch } from '../../hook/useStore';
import { IHouse } from '../model/house.model';
import { selectItem } from '../store';
import pic from './../../../public/image/pic.jpg';
import MenuCard from './MenuCard';

export default function Card({ id, title, city, mount, select }: IHouse) {

  const dispacth = useAppDispatch()
  const selectCard = () => {
    dispacth(selectItem(id));
  }
  return (
    <Flex
      onClick={selectCard}
      borderRadius="20px"
      bg={'white'}
      p={2}
      h="250px"
      w={{ base: '315px', md: '100%' }}
      direction="row"
      gap={8}
      border="5px solid"
      borderColor={select ? 'green.500' : 'gray.50' }
    >
      <Image
        src={pic.src}
        alt="Picture of the author"
        width={350}
        height={350}
        blurDataURL="data:..."
      />
      <Flex w="100%" p={2} direction="column" justifyContent="space-between">
        <Flex direction="column" gap={3}>
          <Flex
            h="38px"
            borderRadius="50%"
            me="12px"
            direction="row"
            justifyContent={'space-between'}
          >
            <Text
              my="auto"
              fontWeight="600"
              color={'gray.800'}
              textAlign="center"
              fontSize="4xl"
              me="auto"
            >
              {title}
            </Text>
            <MenuCard id={id} />
          </Flex>
          <Text
            color={'gray.500'}
            fontWeight="400"
            textAlign="start"
            fontSize="lg"
            w="100%"
          >
            {city}
          </Text>
        </Flex>

        <Flex
          h="38px"
          borderRadius="50%"
          me="12px"
          direction="row"
          justifyContent={'space-between'}
          alignItems={'end'}
        >
          <Flex alignItems={'center'} gap={1}>
            <Text color={'green.700'} fontWeight="800" fontSize="3xl">
              $ {mount}
            </Text>
            <Text color={'gray.500'} fontWeight="800" fontSize="xl">
              /mes
            </Text>
          </Flex>
          <Text color={'gray.700'} fontWeight="400" fontSize="md">
            Agregado recientemente
          </Text>
        </Flex>
      </Flex>
    </Flex>
  );
}
