import { AddIcon } from '@chakra-ui/icons';
import {
  Box,
  Divider,
  Flex,
  Heading,
  List,
  ListIcon,
  ListItem,
  Spacer
} from '@chakra-ui/react';
import Link from 'next/link';

export default function Navegation() {
  return (
    <Box p={6} h={'90%'}>
      <Heading mb={2}> StayApp </Heading>
      <Divider />
      <Flex h={'100%'} flexDirection={'column'}>
        <List spacing={10} mt={6}>
          <ListItem>
            <ListIcon as={AddIcon} color="green.500" />
            <Link href="/">Dashboard</Link>
          </ListItem>
          <ListItem>
            <ListIcon as={AddIcon} color="green.500" />
            <Link href="#">Profile</Link>
          </ListItem>
          <ListItem>
            <ListIcon as={AddIcon} color="green.500" />
            <Link href="#">Listing</Link>
          </ListItem>
          {/* You can also use custom icons from react-icons */}
          <ListItem>
            <ListIcon as={AddIcon} color="green.500" />
            <Link href="#">Setting</Link>
          </ListItem>
        </List>
        <Spacer />
        <Link href="#">LogOut</Link>
      </Flex>
    </Box>
  );
}
