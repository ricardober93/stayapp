import {
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure,
  useToast,
} from '@chakra-ui/react';
import router from 'next/router';
import { useId } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { IHouse } from '../model/house.model';
import { useAddHouseMutation } from '../services/stayApp.service';

export default function CreateModalHouse() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [addHouse, { isLoading }] = useAddHouseMutation();
  const toast = useToast();
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors,},
  } = useForm<IHouse>();
  const onSubmit: SubmitHandler<IHouse> = async (body) => {
    try {
      body.id = Math.floor(Math.random() * 100);
      body.select = false;
      await addHouse(body);
      onClose();
      toast({
        title: 'Creado',
        description: 'creado exitosamente',
        status: 'info',
        duration: 4000,
        isClosable: true,
      });
      reset()
    } catch (error: any) {
      toast({
        title: 'Error',
        description: error?.data.message,
        status: 'error',
        duration: 4000,
        isClosable: true,
      });
    }
  };
  return (
    <>
      <Button colorScheme="blue" onClick={onOpen}>
        Crear nuevo item
      </Button>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <form onSubmit={handleSubmit(onSubmit)}>
            <ModalHeader>Crear un item nuevo</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <FormControl mb={4} isInvalid={errors?.title}>
                <FormLabel htmlFor="name">Nombre del inmueble</FormLabel>
                <Input
                  id="title"
                  placeholder="title"
                  {...register('title', {
                    required: 'This is required',
                    minLength: {
                      value: 4,
                      message: 'Minimum length should be 4',
                    },
                  })}
                />
                <FormErrorMessage>{errors?.title?.message}</FormErrorMessage>
              </FormControl>
              <FormControl mb={4} isInvalid={errors?.city}>
                <FormLabel htmlFor="name">Ciudad</FormLabel>
                <Input
                  id="city"
                  placeholder="city"
                  {...register('city', {
                    required: 'This is required',
                    minLength: {
                      value: 4,
                      message: 'Minimum length should be 4',
                    },
                  })}
                />
                <FormErrorMessage>{errors?.name?.message}</FormErrorMessage>
              </FormControl>

              <FormControl mb={4} isInvalid={errors?.mount}>
                <FormLabel htmlFor="name">Precio</FormLabel>
                <Input
                  id="mount"
                  type={'number'}
                  placeholder="mount"
                  {...register('mount', {
                    valueAsNumber: true,
                    required: 'This is required',
                    min: {
                      value: 5,
                      message: 'Minimum length should be 5',
                    },
                  })}
                />
                <FormErrorMessage>{errors?.mount?.message}</FormErrorMessage>
              </FormControl>
            </ModalBody>

            <ModalFooter display={'flex'} justifyContent="space-between">
              <Button
                variant="ghost"
                colorScheme="red"
                mr={3}
                onClick={onClose}
              >
                Cerrar
              </Button>
              <Button type="submit" isLoading={isLoading} colorScheme="green">
                Crear
              </Button>
            </ModalFooter>
          </form>
        </ModalContent>
      </Modal>
    </>
  );
}
