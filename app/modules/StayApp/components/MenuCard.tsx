import { HamburgerIcon } from '@chakra-ui/icons';
import { Menu, MenuButton, Button, MenuList, MenuItem, useToast } from '@chakra-ui/react';
import { useDeletHouseMutation } from '../services/stayApp.service';

function MenuCard({ id }: string) {
    const toast = useToast();
   const [deleteHouse ] = useDeletHouseMutation()
  const edited = () => {};

  const deleted = () => {
    console.log(id + ' deleted');
    deleteHouse(id).then(() => {
        toast({
            title: 'Eliminado',
            description: 'eliminado exitosamente',
            status: 'success',
            duration: 4000,
            isClosable: true,
          });
    }).catch(() => {
        toast({
            title: 'Error',
            description: 'Fallo al eliminar el item',
            status: 'error',
            duration: 4000,
            isClosable: true,
          });

    });
  };
  return (
    <Menu>
      <MenuButton
        as={Button}
        _hover={{ bg: 'gray.400' }}
        _expanded={{ bg: 'blue.400' }}
        _focus={{ boxShadow: 'outline' }}
      >
        <HamburgerIcon />
      </MenuButton>
      <MenuList>
        <MenuItem onClick={edited}>Edit</MenuItem>
        <MenuItem onClick={deleted}>Delete</MenuItem>
      </MenuList>
    </Menu>
  );
}

export default MenuCard;
