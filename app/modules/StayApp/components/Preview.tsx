import { Avatar, Box, Text, Wrap, WrapItem } from '@chakra-ui/react';
import { useAppSelector } from '../../hook/useStore';
import CreateModalHouse from './CreateModalHouse';

function Preview() {
  const user = useAppSelector((state) => state.user);
  return (
    <Box p={10}>
      <Wrap mb={10}>
        <WrapItem>
          <Text>{user.name}</Text>
        </WrapItem>
        <WrapItem>
          <Avatar
            size="xs"
            name={user.name}
            src="https://bit.ly/dan-abramov"
          />
        </WrapItem>
      </Wrap>

      <CreateModalHouse />
    </Box>
  );
}

export default Preview;
