import { SearchIcon } from '@chakra-ui/icons';
import {
  Box,
  Divider,
  Flex,
  Heading,
  Input,
  InputGroup,
  InputLeftElement,
} from '@chakra-ui/react';
import useList from '../hooks/useList';
import Card from './Card';

export default function List() {
  const { list } = useList();
  return (
    <Box as="main" p={6}>
      <InputGroup bg={'white'} mb={10}>
        <InputLeftElement
          pointerEvents="none"
          children={<SearchIcon color="gray.300" />}
        />
        <Input type="tel" placeholder="Medellin..." />
      </InputGroup>
      <Heading mb={5}> All Listing </Heading>
      <Divider mb={10} />

      <Flex
        h={'70vh'}
        direction="column"
        gap={4}
        scrollBehavior="smooth"
        overflowY={'scroll'}
        sx={{
            '&::-webkit-scrollbar': {
              width: '16px',
              borderRadius: '8px',
              backgroundColor: `rgba(0, 0, 0, 0.03)`,
            },
            '&::-webkit-scrollbar-thumb': {
              backgroundColor: `rgba(0, 0, 0, 0.03)`,
            },
          }}
      >
        {list
          ? list.map((house) => (
              <Card
                key={house.id}
                id={house.id}
                title={house.title}
                city={house.city}
                mount={house.mount}
                select={house.select}
              />
            ))
          : 'Cargando...'}
      </Flex>
    </Box>
  );
}
