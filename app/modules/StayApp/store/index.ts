import { houseAPi } from './../services/stayApp.service';
import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../../store';
import { IHouse } from '../model/house.model';

// Define a type for the slice state
interface StayAppState {
  house: IHouse[];
}

// Define the initial state using that type
const initialState: StayAppState = {
  house: [],
};

export const stayAppSlice = createSlice({
  name: 'stayApp',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    selectItem: (state, {payload}) => {
      const houseIdx = state.house.findIndex( house => house.id === payload)
      state.house[houseIdx].select = !state.house[houseIdx].select;
      
    },
    remove: (state) => {},
  },
  extraReducers: (builder) => {
    builder.addMatcher(
      houseAPi.endpoints.getHouses.matchFulfilled,
      (state, { payload }) => {
        state.house = payload;
      },
    );
  },
});

export const { selectItem, remove } = stayAppSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.stayApp;

export default stayAppSlice.reducer;
