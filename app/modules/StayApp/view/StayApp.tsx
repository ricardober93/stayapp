import { Box, HStack } from '@chakra-ui/react';
import Navegation from '../components/Navegation';
import Preview from '../components/Preview';
import useLogIn from '../hooks/useLogIn';

type Props = {
  children?:
    | JSX.Element
    | JSX.Element[]
    | string
    | string[];
};

export default function StayApp({ children }: Props) {

  useLogIn()
  return (
    <Box h={'100vh'}>
      <HStack h={'100%'} spacing="" alignItems={'flex-start'}>
        <Box w="20%" h={'100%'}>
          <Navegation />
        </Box>
        <Box w="60%" bg="gray.100" h={'100%'}>
          { children }
        </Box>
        <Box w="20%" h={'100%'}>
          <Preview />
        </Box>
      </HStack>
    </Box>
  );
}
