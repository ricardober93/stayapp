import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../../store';
import { IUser } from '../model/user.model';
import { authApi } from '../services/login.service';


// Define the initial state using that type
const initialState: IUser = {
    name: undefined,
    isActive: true,
    isLoggin: false,
};

export const userSlice = createSlice({
  name: 'user',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    logIn: (state) => {},
    logOut: (state) => {},
  },
  extraReducers: (builder) => {
    builder.addMatcher(
      authApi.endpoints.login.matchFulfilled,
      (state, { payload }) => {
        state.name = payload.name
        state.Roles = payload.Roles,
        state.isLoggin = true,
        state.isActive = true,
        state.permission = []
      }
    )
  },
});

export const { logIn, logOut } = userSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.user;

export default userSlice.reducer;
