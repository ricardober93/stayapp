import { useToast } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { SubmitHandler, useForm } from 'react-hook-form';
import { useLoginMutation } from '../services/login.service';

export type Inputs = {
  user: string;
  password: string;
};

export default function useFormLogin() {
  const toast = useToast();
  const router = useRouter()
  const [login, { isLoading }] = useLoginMutation();
  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm<Inputs>();
  const onSubmit: SubmitHandler<Inputs> = async (body) => {
    try {

      await login(body).unwrap();
      router.push('/')
    } catch (error: any) {
      toast({
        title: 'Error',
        description: error?.data.message,
        status: 'error',
        duration: 4000,
        isClosable: true,
      });
    }
  };

  return {
    onSubmit,
    handleSubmit,
    register,
    errors,
    isSubmitting,
    isLoading,
  };
}
