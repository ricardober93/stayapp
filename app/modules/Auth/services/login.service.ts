import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import type { Inputs } from '../hooks/useFormLogin';
import { IUser } from './../model/user.model';

export const authApi = createApi({
  reducerPath: 'authApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:8080/auth' }),
  endpoints: (builder) => ({
    login: builder.mutation<IUser, Inputs>({
      query: (body?: Inputs) => ({
        url: 'login',
        method: 'post',
        body,
      }),
    }),
  }),
});

export const { useLoginMutation } = authApi;
