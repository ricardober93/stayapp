export interface IUser {
    name?: string,
    isActive: boolean,
    isLoggin: boolean
    Roles?: IRoles[],
    permission?: IPermission[]
}

export interface IRoles {
    name: string,
}

export interface IPermission {
    name: string,
}