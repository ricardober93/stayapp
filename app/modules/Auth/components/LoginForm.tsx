import {
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  Input,
  StackDivider,
  VStack
} from '@chakra-ui/react';
import useFormLogin from '../hooks/useFormLogin';

export default function LoginForm() {
  const { onSubmit, handleSubmit, register, errors, isLoading } =
    useFormLogin();
  return (
    <form onSubmit={handleSubmit(onSubmit)}>

      <VStack
        border={1}
        borderColor={'gray.400'}
        borderStyle={'solid'}
        borderRadius={'md'}
        p={8}
        boxShadow="lg"
        divider={<StackDivider borderColor="gray.50" />}
        spacing={4}
        align="stretch"
      >
        <Heading as="h3" size="lg">
          Bienvenido
        </Heading>

        <FormControl isInvalid={errors?.user}>
          <FormLabel htmlFor="name">Usuario</FormLabel>
          <Input
            id="user"
            placeholder="user"
            {...register('user', {
              required: 'This is required',
              minLength: { value: 4, message: 'Minimum length should be 4' },
            })}
          />
          <FormErrorMessage>{errors?.name?.message}</FormErrorMessage>
        </FormControl>
        <FormControl isInvalid={errors?.password}>
          <FormLabel htmlFor="name">Contraseña</FormLabel>
          <Input
            id="password"
            placeholder="password"
            type="password"
            {...register('password', {
              required: 'This is required',
              minLength: { value: 4, message: 'Minimum length should be 4' },
            })}
          />
          <FormErrorMessage>{errors?.password?.message}</FormErrorMessage>
        </FormControl>

        <Button mt={4} colorScheme="teal" isLoading={isLoading} type="submit">
          Iniciar sesión
        </Button>
      </VStack>
    </form>
  );
}
