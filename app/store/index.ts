import { configureStore } from '@reduxjs/toolkit'
import { authApi } from '../modules/Auth/services/login.service';
import { houseAPi } from '../modules/StayApp/services/stayApp.service';
import stayAppSlice from '../modules/StayApp/store'
import userSlice  from './../modules/Auth/store/index';
import logger from './middleware/logger'

export const store = configureStore({
  reducer: {
    stayApp: stayAppSlice,
    user: userSlice,
    [authApi.reducerPath]: authApi.reducer,
    [houseAPi.reducerPath]: houseAPi.reducer,
  }
  ,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware()
      // prepend and concat calls can be chained
      .concat(logger, authApi.middleware, houseAPi.middleware)
})
// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch