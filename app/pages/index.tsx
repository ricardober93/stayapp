import { Box } from '@chakra-ui/react'
import type { NextPage } from 'next'
import Head from 'next/head'
import List from '../modules/StayApp/components/List'
import StayApp from '../modules/StayApp/view/StayApp'

const Home: NextPage = () => {
  return (
    <Box>
      <Head>
        <title>StayApp</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <StayApp> 
        <List />
      </StayApp>
    </Box>
  )
}

export default Home
