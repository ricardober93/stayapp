import { Box, Center } from '@chakra-ui/react';
import LoginForm from '../modules/Auth/components/LoginForm';

export default function login() {
  return (
    <Box w="100%" h="100vh">
      <Center h="100vh">
        <LoginForm />
      </Center>
    </Box>
  );
}
